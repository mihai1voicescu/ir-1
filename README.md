# Information retrieval project 1

## Components

### Frontend
Frontend can be found in the `frontend` folder.
We use basic JavaScript and HTML to create the register and single page application.

### Backend
Backend can be found in the `backend` folder.
Done in Nodejs. Exposes rest services and provides the static files.

### Setup
In `setup/README.md` the mapping for the Elasticsearch indices can be found.
To load data in the cluster either run the `setup.js` script if the AQUAINT folder is in the root directory or `npm run script load_pages_no_mapping` if a dump is available.
After this run the `set_up_tasks.js` script and `set_up_scores.js`.
The `build_optimal_queries.js` script can beb used to generate the questions. These are found in `final.json`.


## How to run
* `npm install`
* Make sure the setup is complete by following the steps in the `Setup`.
* `npm start`
