import numpy as np 
import matplotlib.pyplot as plt

task_303 = [0.03234, 0.4259, 0.0298, 0.5103, 0.0905, 0.0323, 0, 0.0905, 0.0905, 0.0843, 0.1536, 0.2953]
task_362 = [0.1115, 0.05493, 0, 0.1799, 0.24, 0.1799, 0.3766, 0, 0.0379, 0.1142, 0.1725, 0.2968]
task_367 = [0.6119, 0, 0, 0.3685, 0.0162, 0.1627, 0, 0.0038, 0.5111, 0.3812, 0.3452, 0.3205]
task_375 = [0.1166, 0.2947, 0.0516, 0.0467, 0.1208, 0.0967, 0, 0.4808, 0.2952, 0.5394, 0.2793, 0.3915]
task_378 = [0, 0.0083, 0, 0.0404, 0.0416, 0, 0, 0.1535, 0.4081, 0.4349, 0.3925, 0.5725]
task_383 = [0, 0, 0, 0.0077, 0.1715, 0.0486, 0.0666, 0.0085, 0.4764, 0.4975, 0.3869, 0.6361]
task_394 = [0.335, 0.5065, 0.2127, 0.9211, 0.5490, 0.0590, 0.0733, 0.0845, 0.8926, 0.6926, 0.5251, 0.6395]
task_401 = [0.7756, 0.0029, 0.0082, 0.01, 0.0277, 0.0236, 0, 0, 0.6515, 0.6323, 0.6257, 0.7935]
task_426 = [0.0094, 0.0416, 0, 0.3887, 0.4320, 0.2795, 0.0384, 0.1776, 0.4479, 0.4053, 0.4629, 0.5025]
task_622 = [0.3282, 0.0684, 0.2066, 0.3589, 0.4715, 0.6446, 0.2876, 0.0323, 0.6125, 0.4294, 0.5183, 0.6021]
task_638 = [0.0033, 0.2312, 0.1796, 0.7179, 0.1286, 0.03, 0.2822, 0.0112, 0.051, 0.2746, 0.5272, 0.6295]
task_639 = [0.0091, 0.2298, 0.0504, 0.0055, 0.0975, 0.2016, 0.1807, 0.055, 0.0756, 0.3953, 0.4682, 0.4617]
task_689 = [0, 0.4387, 0.0097, 0.2891, 0.5892, 0.1100, 0.2166, 0.5117, 0.0538, 0.253, 0.2153, 0.1692]


#The tasks are as following: [low,control,control,low,control,low,control,low, high, high, high, high]

#boxplot over average precision to detect fatigue

low = []
high = []
control = []

#print(type(task_303))

task_303 = np.array(task_303)
task_689 = np.array(task_689)
task_362 = np.array(task_362)
task_367 = np.array(task_367)
task_375 = np.array(task_375)
task_378 = np.array(task_378)
task_383 = np.array(task_383)
task_394 = np.array(task_394)
task_401 = np.array(task_401)
task_426 = np.array(task_426)
task_622 = np.array(task_622)
task_638 = np.array(task_638)
task_639 = np.array(task_639)
all = np.vstack((task_303,task_689,task_362, task_367,task_375, task_378, task_383, task_394, task_401, task_426, task_622, task_638, task_639));


mean_on_col = np.mean(all, axis = 0) 

tasks = all.T

plt.boxplot(tasks)
plt.ylabel('Average Precision')
plt.xlabel('Task Number')
plt.show()


#scatterplot over the 3 groups

only_test_study = np.vstack((task_622, task_638, task_639))




task_622_original = [0.3282, 0.0684, 0.2066, 0.3589, 0.4715, 0.6446, 0.2876, 0.0323, 0.6125, 0.4294, 0.5183, 0.6021]
task_638_original = [0.0033, 0.2312, 0.1796, 0.7179, 0.1286, 0.03, 0.2822, 0.0112, 0.051, 0.2746, 0.5272, 0.6295]
task_639_original = [0.0091, 0.2298, 0.0504, 0.0055, 0.0975, 0.2016, 0.1807, 0.055, 0.0756, 0.3953, 0.4682, 0.4617]

low_622 = [0.3282, 0.3589, 0.6446, 0.0323]
low_638 = [0.0033,0.7179, 0.03, 0.0112]
low_639 = [0.0091, 0.0055, 0.2016, 0.055]

lw_622 = np.mean(low_622)
lw_638 = np.mean(low_638)
lw_639 = np.mean(low_639)

low = [lw_622, lw_638, lw_639]


high_622 = [0.6125, 0.4294, 0.5183, 0.6021]
high_638 = [0.051, 0.2746, 0.5272, 0.6295]
high_639 = [0.0756, 0.3953, 0.4682, 0.4617]
hg_622 = np.mean(high_622)
hg_638 = np.mean(high_638)
hg_639 = np.mean(high_639)

high = [hg_622, hg_638, hg_639]


control_622 = [0.0684, 0.2066,0.4715, 0.2876]
ctr_622 = np.mean(control_622)
control_638 = [0.2312, 0.1796, 0.1286, 0.2822]
ctr_638 = np.mean(control_638)
control_639 = [0.2298, 0.0504, 0.0975, 0.1807]
ctr_639 = np.mean(control_639)

control = [ctr_622, ctr_638, ctr_639]

x = [1, 2, 3]
plt.xlabel('task number')
plt.ylabel('average precision')
plt.scatter(x,high,marker = 'o', label='High')
plt.scatter(x,low, marker = 'v', label='Low')
plt.scatter(x,control,marker='s', label='Control')

plt.legend(framealpha=1, frameon=True);


plt.show()

#comparison between the average precisions of the 3 groups by boxplots


low = []
low.append(mean_on_col[0])
low.append(mean_on_col[3])
low.append(mean_on_col[5])
low.append(mean_on_col[7])

high = []
high.append(mean_on_col[8])
high.append(mean_on_col[9])
high.append(mean_on_col[10])
high.append(mean_on_col[11])

control = [] 
control.append(mean_on_col[1])
control.append(mean_on_col[2])
control.append(mean_on_col[4])
control.append(mean_on_col[6])


data = [low, control, high]
plt.ylabel('Average Precision')
plt.boxplot(data)
plt.show()





#check if suggestions improve

#for high group

task_303_high = [0.0905, 0.0843, 0.1536, 0.2953]
task_689_high = [0.0538, 0.253, 0.2153, 0.1692]
task_362_high = [0.0379, 0.1142, 0.1725, 0.2968]
task_367_high = [0.5111, 0.3812, 0.3452, 0.3205]
task_375_high = [0.2952, 0.5394, 0.2793, 0.3915]
task_378_high = [0.4081, 0.4349, 0.3925, 0.5725]
task_383_high = [0.4764, 0.4975, 0.3869, 0.6361]
task_394_high = [0.8926, 0.6926, 0.5251, 0.6395]
task_401_high = [0.6515, 0.6323, 0.6257, 0.7935]
task_426_high = [0.4479, 0.4053, 0.4629, 0.5025]
task_622_high = [0.6125, 0.4294, 0.5183, 0.6021]
task_638_high = [0.051, 0.2746, 0.5272, 0.6295]
task_639_high = [0.0756, 0.3953, 0.4682, 0.4617]

task_303_low = [0.03234, 0.5103, 0.0323, 0.0905]
task_689_low = [0, 0.4387, 0.5892, 0.2166]
task_362_low = [0.1115, 0.1799, 0.1799, 0.0379]
task_367_low = [0.6119, 0.3685, 0.1627, 0.0038]
task_375_low = [0.1166, 0.0467, 0.0967, 0.4808]
task_378_low = [0, 0.0404, 0, 0.1535]
task_383_low = [0, 0.0077, 0.0486, 0.0085]
task_394_low = [0.335, 0.9211, 0.0590, 0.0845]
task_401_low = [0.7756, 0.01, 0.0236, 0]
task_426_low = [0.0094, 0.4320,  0.0384, 0.4479]
task_622_low = [0.3282,0.3589, 0.6446,  0.0323]
task_638_low = [0.0033, 0.7179, 0.03, 0.0112]
task_639_low = [0.0091, 0.0055, 0.2016, 0.055]


before_training_high = np.vstack((task_303_high, task_689_high, task_362_high, task_367_high, task_375_high))
training_high = np.vstack((task_378_high, task_383_high, task_394_high, task_401_high, task_426_high))
test_high = np.vstack((task_622_high, task_638_high, task_639_high))

btraining_mean_high = np.mean(before_training_high, axis = 0)
training_mean_high = np.mean(training_high, axis = 0)
test_mean_high = np.mean(test_high, axis = 0)


data = [btraining_mean_high, training_mean_high, test_mean_high]
plt.ylabel('Average Precision')
plt.boxplot(data)
plt.show()

#for low group




task_303 = np.array(task_303)
task_689 = np.array(task_689)
task_362 = np.array(task_362)
task_367 = np.array(task_367)
task_375 = np.array(task_375)
task_378 = np.array(task_378)
task_383 = np.array(task_383)
task_394 = np.array(task_394)
task_401 = np.array(task_401)
task_426 = np.array(task_426)
task_622 = np.array(task_622)
task_638 = np.array(task_638)
task_639 = np.array(task_639)

before_training_low = np.vstack((task_303_low, task_689_low, task_362_low, task_367_low, task_375_low))
training_low = np.vstack((task_378_low, task_383_low, task_394_low, task_401_low, task_426_low))
test_low = np.vstack((task_622_low, task_638_low, task_639_low))

btraining_mean_low = np.mean(before_training_low, axis = 0)
training_mean_low = np.mean(training_low, axis = 0)
test_mean_low = np.mean(test_low, axis = 0)

data = [btraining_mean_low, training_mean_low, test_mean_low]
plt.ylabel('Average Precision')
plt.boxplot(data)
plt.show()

