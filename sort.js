let a = require("./unfiltered");

a.forEach(e => {
    e.terms.sort((a,b) => b.avp - a.avp);
    if (e.terms.length > 100)
        e.terms.length = 100;
});

console.log(JSON.stringify(a));
