# Mappings

```
PUT page
{
    "settings": {
        "index": {
          "similarity": {
            "my_similarity" : {
                "type" : "LMJelinekMercer",
                "lambda" : 0.1
              }
          },
          "analysis": {
                "analyzer": {
                  "my_custom_analyzer": {
                    "type":      "custom", 
                    "tokenizer": "standard",
                    "char_filter": [
                      "html_strip"
                    ],
                    "filter": [
                      "asciifolding",
                      "lowercase",
                      "apostrophe",
                      "classic"
                    ]
                  }
                }
              }
        },
        "number_of_shards": 1
      },
    "mappings" : {
        "_doc" : {
            "properties" : {
                "timestamp" : { "type" : "date" },
                "headline": {"type": "text", "similarity": "my_similarity","analyzer":"my_custom_analyzer"},
                "body": {"type": "text", "similarity": "my_similarity","analyzer":"my_custom_analyzer"},
                "source": {"type": "keyword"},
                "year": {"type": "integer"},
                "taskScore": {"type": "object"}
            }
        }
    }
}
```

```
PUT user
{
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "_doc" : {
            "properties" : {
                "dateOfBirth" : { "type" : "date" },
                "nickname": {"type": "keyword"},
                "profesion": {"type": "keyword"},
                "sex": {"type": "keyword"},
                "totalScore": {"type": "float"},
                "taskScore": {"type": "object"},
                "taskType": {"type":"keyword"},
                "created": {"type":"date"}
            }
        }
    }
}
```

```
PUT task
{
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "_doc" : {
            "properties" : {
                "description" : { "type" : "text" },
                "title" : { "type" : "text" },
                "narative" : { "type" : "text" }
            }
        }
    }
}
```
