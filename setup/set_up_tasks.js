'use strict';

const fs = require('fs');
const elasticsearch = require('elasticsearch');
const client = new elasticsearch.Client({
    host: 'localhost:9200'
});

const cheerio = require('cheerio');

let data = fs.readFileSync(__dirname + "/topics.txt", 'utf8');

// let $  = cheerio.load(data);

let noErrors = true;

data.replace(/<top>([^]*?)<\/top>/g, function (m, content) {
    content = content.replace("<num>", "").trim();

    content = content.split("<title>");

    let id = content[0].trim().match(/Number: (\d+)/)[1];

    content = content[1]. split("<desc>");

    let title = content[0].trim();



    content = content[1]. split("<narr>");

    let description = content[0].trim();

    let narrative = content[1].trim();

    console.log(require('util').inspect({id, title, description, narrative}, {depth: null, colors: true}));

    if (!id || ! title || ! description || ! narrative)
        throw new Error("Ups");

    client.index({
        id,
        index: "task",
        type: "_doc",
        body: {
            title, description, narrative
        }
    }, function (err) {
        if (err) {
            console.error(err);
            noErrors = false;
        }

        if (noErrors)
            console.log("Everything is fine");
    })
});
