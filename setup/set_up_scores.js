'use strict';

const fs = require('fs');
const elasticsearch = require('elasticsearch');
/**
 * @type {Client}
 */
const client = new elasticsearch.Client({
    host: 'localhost:9200'
});

let toWrite = [];
let queue = [];

let data = fs.readFileSync(__dirname + "/rank.txt", 'utf8');

let noErrors = true;

data = data.split("\n");
let lastId = -1, sum = 0, pageIds = [];
for (let row of data) {
    row = row.trim();
    if (row === "")
        continue;
    let tokens = row.split(/ /g);
    let taskId = tokens[0];
    let pageId = tokens[2];
    let relevance = +tokens[3];
    if (lastId === -1) {
        lastId = taskId;
    } else if (lastId !== taskId) {
        let tId = lastId;
        buildOptimalQueries(pageIds, tId);

        lastId = taskId;
        pageIds = [];
        sum = 0;
    }
    if (relevance !== 0) {
        sum += relevance;
        pageIds.push(pageId);

        let taskScore = {};
        taskScore[taskId] = relevance;

        if (!pageId)
            console.log(row);

        queueResult({
            index: "page",
            type: "_doc",
            id: pageId,
            body: {
                taskScore
            }
        });

    }
}

function queueResult({index, type, id, body}) {
    queue.push({update: {_index: index, _type: type, _id: id}}, {doc: body});

    if (queue.length > 500)
        emptyQueue();


}

function emptyQueue() {
    if (queue.length) {
        let save = queue;
        client.bulk({body: queue}, function (err, res) {
            if (err || res.errors) {
                console.log(save);
                console.error("ERRED");
                if (res.errors)
                    console.error(require('util').inspect(res.items, {depth: null, colors: true}));
                else
                    console.error(err);

                noErrors = false;
            }

            if (noErrors)
                console.log("Everything is fine");
        });

    }


    // setTimeout(function () {
    //     console.log(require('util').inspect(queue, {depth: null, colors: true}));
    // });
    queue = [];
}

function buildOptimalQueries(pageIds, taskId) {
    toWrite.push({pageIds, taskId});
}

process.once("beforeExit", function () {

    emptyQueue();


    process.once("exit", function () {
        let r = JSON.stringify(toWrite);
        console.log(r);
        fs.writeFileSync(__dirname + "/tasks.json", r);
    });

});
