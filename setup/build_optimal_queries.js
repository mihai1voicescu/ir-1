'use strict';

const fs = require('fs');
const elasticsearch = require('elasticsearch');
/**
 * @type {Client}
 */
const client = new elasticsearch.Client({
    host: 'localhost:9200',
    keepAlive: false
    // log: 'trace'
});

const RESULTS_SIZE = 50;

let tasks = require("./tasks.json");

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

class CountMap {
    constructor() {
        this.map = new Map();
    }

    add(key) {
        this.map.set(key, (this.map.get(key) || 0) + 1);
    }

    getResults() {
        let m = [];

        this.map.forEach(function (value, key) {
            m.push({key, value});
        });

        m.sort((a, b) => b.value - a.value);

        m.length = 100;

        return m.map(e => {
            return {term: e.key}
        });
    }
}

const stopwords = new Set(['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't"]);


let noErrors = true;
let COUNT = 0;
let acc = [
    303,
    310,
    362,
    367,
    375,
    378,
    383,
    394,
    401,
    426,
    622,
    638,
    639,
    651,
    689];
tasks = tasks.filter(e => acc.includes(+e.taskId));

for (let {pageIds, taskId} of tasks)
    buildOptimalQueries(pageIds, taskId).then(function () {
        console.log("Finish results for " + taskId);
    }).catch(function (err) {
        noErrors = false;
        console.error(`Unable to finish ${taskId} due to ${err}:
            ${JSON.stringify(err)}`);
        console.error(err);
    });

async function buildOptimalQueries(pageIds, taskId) {
    let terms1 = new Set();
    let promises = [];

    for (let i = 0; i < pageIds.length; i += 5) {
        let ids = pageIds.slice(i, i + 5);
        promises.push(client.mtermvectors({
            ids,
            index: "page",
            type: "_doc",
            fields: ["body", "headline"],
            "offsets": false,
            "payloads": false,
            "positions": false,
            "term_statistics": false,
            "field_statistics": false
        }));
    }

    let results = await Promise.all(promises);

    console.log(++COUNT);

    let counterClass = new CountMap();
    for (let {docs} of results)
        for (let d of docs) {
            if (d.found === false) {
                console.error(`Failed to find document ${d._id}`);
            } else {
                let terms = [];

                try {
                    if (d.term_vectors.body)
                        terms = Object.keys(d.term_vectors.body.terms);
                    else
                        console.error("NO BODY");
                    if (d.term_vectors.headline)
                        terms = terms.concat(Object.keys(d.term_vectors.headline.terms));
                    else
                        console.error("NO HEADLINE");
                } catch (e) {
                    throw e;
                }

                terms.forEach(e => {
                    if (!stopwords.has(e) && !/^\s*www\./.test(e))
                        counterClass.add(e);
                    // else console.log(e);
                });


            }
        }

    let body = [];
    terms1 = counterClass.getResults();

    // for (let query of terms1) {
    //     if (query == null)
    //         console.log("WTF>>");
    //     body.push({index: 'page', type: '_doc'},
    //         {
    //             "_source": "taskScore." + taskId,
    //             "size": RESULTS_SIZE,
    //             "query": {
    //
    //                 "multi_match": {
    //                     query,
    //                     "fields": ["body", "headline"]
    //                 }
    //             }
    //         }
    //     );
    // }
    //
    //
    let selected1Terms = terms1;
    // let counts = 0;
    let BATCH_SIZE = 1000;
    // while (body.length) {
    //     let b = body.splice(0, BATCH_SIZE);
    //
    //     /**
    //      *
    //      * @type {MSearchResponse<any>}
    //      */
    //     let response;
    //     try {
    //         response = await client.msearch({body: b});
    //     } catch (e) {
    //         throw e;
    //     }
    //
    //     for (let k = 0; k < response.responses.length; k++) {
    //         let res = response.responses[k];
    //         let avp = 0;
    //         let relevantCount = 1;
    //
    //         for (let i = 0; i < res.hits.hits.length; i++) {
    //             if (res.hits.hits[i]._source.taskScore && res.hits.hits[i]._source.taskScore[taskId])
    //                 avp += relevantCount++ / (i + 1);
    //         }
    //         avp /= RESULTS_SIZE;
    //         let term = terms1[k + BATCH_SIZE / 2 * counts];
    //
    //         if (term == null)
    //             console.log("WTF");
    //         selected1Terms.push({
    //             term,
    //             avp
    //         })
    //     }
    //
    //     selected1Terms.sort((a, b) => b.avp - a.avp);
    //     selected1Terms.length = 100;
    //     counts++;
    // }


    let currentTerms = selected1Terms;
    let newTerms = [];
    let toWrite;
    let marks = [];
    for (let len = 1; len < 4; len++) {
        let body = [];
        let requestTerms = [];
        let requestTermsSet = new Set();

        for (let query1 of currentTerms) {
            query1 = query1.term;
            let counts = 0;
            for (let t of selected1Terms) {
                counts++;
                t = t.term;
                if (!query1.includes(t)) {
                    let ar = query1.split(/ /g);
                    ar.push(t);
                    ar.sort();
                    let tr = ar.join(" ");
                    if (!requestTermsSet.has(tr)) {
                        requestTerms.push(tr);
                        requestTermsSet.add(tr);
                        body.push({index: 'page', type: '_doc'},
                            {
                                "size": RESULTS_SIZE,
                                "_source": "taskScore." + taskId,
                                "query": {
                                    "multi_match": {
                                        query: tr,
                                        "fields": ["body", "headline"]
                                    }
                                }
                            }
                        );
                    }
                }
            }
            marks.push(counts);
        }

        let selectedTerms = [];
        let nextBatchIndex = marks.shift();
        let counts = 0;
        while (body.length) {
            let b = body.splice(0, BATCH_SIZE);
            let currentBatch = requestTerms.splice(0, BATCH_SIZE / 2);
            /**
             *
             * @type {MSearchResponse<any>}
             */
            let response = await client.msearch({body: b, requestTimeout: Infinity});


            for (let k = 0; k < response.responses.length; k++) {
                let res = response.responses[k];
                let avp = 0;
                let relevantCount = 1;

                for (let i = 0; i < res.hits.hits.length; i++) {
                    if (res.hits.hits[i]._source.taskScore && res.hits.hits[i]._source.taskScore[taskId])
                        avp += relevantCount++ / (i + 1);
                }
                avp /= RESULTS_SIZE;

                if (counts++ >= nextBatchIndex) {
                    nextBatchIndex = marks.shift();
                    selectedTerms.sort((a, b) => b.avp - a.avp);
                    if (selectedTerms.length > 10)
                        selectedTerms.length = 10;

                    newTerms.push(...selectedTerms);
                    selectedTerms = [];
                }

                let a = {
                    term: currentBatch[k],
                    avp
                };
                selectedTerms.push(a);

            }
            if (selectedTerms.length) {
                selectedTerms.sort((a, b) => b.avp - a.avp);
                if (selectedTerms.length > 10)
                    selectedTerms.length = 10;

                newTerms.push(...selectedTerms);
                selectedTerms = [];
            }

        }


        console.log("DONE " + len);
        currentTerms = newTerms;
        newTerms = [];
        toWrite = {
            taskId,
            terms: currentTerms
        };
        console.log(JSON.stringify(toWrite));

    }

    console.log(JSON.stringify(toWrite));
    fs.appendFileSync("results.txt", JSON.stringify(toWrite));
}
