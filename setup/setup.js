const fs = require('fs');
const path = require('path');
const elasticsearch = require('elasticsearch');
const client = new elasticsearch.Client({
    host: 'localhost:9200'
});


let body = [];

function queueToEs(done) {
    if (body.length > 500) {
        client.bulk({
            body
        }, (err, res) => {
            if (err || res.errors) {
                console.error("ERRED");
                if (res.errors)
                    console.error(require('util').inspect(res.items, {depth: null, colors: true}));
                else
                    console.error(err);
            }


            done();
        });
        body = [];
    } else done();
}


const cheerio = require('cheerio');

function walkDir(dir, callback, done) {
    let ar = fs.readdirSync(dir);

    next();

    function next() {
        if (!ar.length)
            return done();

        let f = ar.shift();
        if (f[0] === '.')
            return next();
        let dirPath = path.join(dir, f);
        let stats = fs.statSync(dirPath);
        let isDirectory = stats.isDirectory();
        isDirectory ?
            walkDir(dirPath, callback, next) : callback(path.join(dir, f), next);
    }
}

function sanitize(string) {
    return string && string.trim();
}

function loadInEs(file, done) {
    let data = fs.readFileSync(file, 'utf8');

    const $ = cheerio.load(data);
    let [_, source, year] = file.match(/.*\/(.*?)\/(.*?)\/(.*?)$/);

    if (!["APW", "NYT", "XIE"].includes(source))
        return;

    let pages = [];
    $('DOC').each(function () {
        let docNode = $(this);

        /**
         *
         * @type {PageDoc}
         */
        let page = {source, year};

        let id = sanitize(docNode.find("DOCNO").text());

        page.timestamp = new Date(docNode.find("DATE_TIME").text());
        page.category = sanitize(docNode.find("CATEGORY").text());
        page.body = sanitize(docNode.find("TEXT").text());
        page.headline = sanitize(docNode.find("HEADLINE").text());
        // page.year =
        // page.source =

        body.push({index: {_index: 'page', _type: '_doc', _id: id}}, page);
    });

    queueToEs(done);
}

walkDir(__dirname + '/../AQUAINT', loadInEs, () => console.log("DONE"));


