'use strict';


const BACKEND_ROUTE = '/backend/';

function getQueryString(q) {
    let terms = [];
    for (let k in q) {
        terms.push( k + "=" + q[k]);
    }

    return "?" + terms.join("&")
}

function htmlInNode(html, node) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    node.appendChild(template.content.firstChild);
}

/**
 *
 * @return {Promise<Task>}
 */
async function getTask() {
    /**
     *
     * @type {Task}
     */
    let task;
    let result = await fetch(BACKEND_ROUTE + "task");

    if (result.ok) {
        task = result.json();
    }
    else {
        document.location.href = "https://docs.google.com/forms/d/e/1FAIpQLSc5hIcOBGzMBwTslQamVDpK1cBVC41ega8oTfsJ6co3D4T-jQ/viewform?usp=pp_url&entry.157913516=" + localStorage.getItem("login");
    }

    return task;
}

async function submitRegister(registerRequest, link) {


    let result = await fetch(BACKEND_ROUTE + "register", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(registerRequest)
        }
    );

    let id = await result.text();
    localStorage.setItem("login", id);

    document.location.href = link;
}

/**
 *
 * @param query
 * @param task
 * @return {Promise<SearchResults>}
 */
async function doSearch(query, task) {
    let qs = {query, task};
    /**
     *
     * @type {Response}
     */
    let res = await fetch(BACKEND_ROUTE + "search" + getQueryString(qs));


    return res.json()
}


