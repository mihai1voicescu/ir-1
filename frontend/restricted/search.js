let id;

let searchBar;
let searchButton;
/**
 * @type HTMLElement
 */
let searchResult;
let taskElement;
let suggestionElement;
let nextTaskButton;

async function getTaskHandler() {
    /**
     * @type {Task}
     */
    let task = await getTask();

    // document.getElementById("description").value = task.description;
    // document.getElementById("narrative").value = task.narrative;
    // document.getElementById("title").value = task.title;
    id = task.id;
    let m;
    if ((m = /^Description:\s([^]*)/.exec(task.description)))
        task.description = m[1];
    if ((m = /^Narrative:\s([^]*)/.exec(task.narrative)))
        task.narrative = m[1];

    task.title = task.title[0].toUpperCase() + task.title.slice(1);

    htmlInNode(`
    <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">${task.title}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${task.description}</h6>
        <p class="card-text">${task.narrative}</p>
      </div>
    </div>
    `, taskElement);

    if (task.suggestions)
        htmlInNode(`<p>Suggestions: ${task.suggestions.map(e => '"' + e.term + '"').join(" ")}</p>`,
            suggestionElement);
}

getTaskHandler();


document.addEventListener("DOMContentLoaded", () => {
    searchBar = document.getElementById("query");
    searchButton = document.getElementById("search-button");
    searchResult = document.getElementById("search-result");
    taskElement = document.getElementById("task");
    nextTaskButton = document.getElementById("next-task");
    suggestionElement = document.getElementById("suggestions");

    searchBar.addEventListener("keyup", function (event) {
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Trigger the button element with a click
            searchButton.click();
        }
    });

});

async function doClick() {
    /**
     *
     * @type {SearchResults}
     */
    let response = await doSearch(searchBar.value, id);

    let resArray = [];
    for (let res of response.results) {
        let relevant = (res.taskScore && res.taskScore[id]) ? '<p class="text-success">Relevant</p>' : '<p class="text-danger">Not relevant</p>';
        resArray.push(`
        <div>
        <h3>${res.headline}</h3>
        ${relevant}
        <p>${res.body}</p>
        </div>
        `)
    }

    let html = `<div>

    <h2>
    Results
    </h2>
    <p>Score ${response.averageScore}</p>
    <div>${resArray.join('\n')}</div>
</div>
    `;

    nextTaskButton.disabled = false;


    htmlInNode(html, searchResult);
}


function clearForNext() {
    searchBar.value = "";
    nextTaskButton.disabled = true;
    while (searchResult.firstChild) {
        searchResult.removeChild(searchResult.firstChild);
    }
    while (taskElement.firstChild) {
        taskElement.removeChild(taskElement.firstChild);
    }
    while (suggestionElement.firstChild) {
        suggestionElement.removeChild(suggestionElement.firstChild);
    }

    getTaskHandler();

}
