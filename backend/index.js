'use strict';

const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const {decorateApp} = require('@awaitjs/express');

const app = express();
const port = 80;

const elasticsearch = require('elasticsearch');

const MAX_TASK = 20;

const SEARCH_SIZE = 20;

const suggestions = new Map();
{
    let json = require("../final");
    for (let j of json) {
        suggestions.set(j.taskId, j.terms);
    }
}

function getTop3(taskId) {
    return suggestions.get(taskId).slice(0, 3);
}

function getBot3(taskId) {
    let a = suggestions.get(taskId);
    return a.slice(a.length - 3);
}

const TASKS = [
    303,
    310,
    362,
    367,
    375, // no suggestions
    378,
    383,
    394,
    401,
    426, // suggestions
    622,
    638,
    639,
    651
];
const SUGGESTIONS_RANGE = [375, 426];

/**
 *
 * @param req
 * @return {Promise<User>}
 */
async function getUserInfo(req) {
    /**
     * @type {GetResponse<User>}
     */
    let userDoc;
    try {
        userDoc = await client.get({
            index: 'user',
            type: '_doc',
            id: req.userId + ""
        });
    } catch (e) {
        console.error("Can not find user " + req.userId);
        throw e;
    }

    /**
     * @type {User}
     */
    let user = userDoc._source;

    return user;
}

/**
 *
 * @type {Client}
 */
const client = new elasticsearch.Client({
    host: 'localhost:9200'
});


app.use(cookieParser());
app.use(bodyParser.json());


const bknd = decorateApp(express());


bknd.use(cookieParser());
bknd.use(bodyParser.json());


app.post('/backend/register', (req, res, next) => {
    /**
     * @type User
     */
    let body = req.body;
    body.dateOfBirth = new Date(body.dateOfBirth);
    body.create = new Date();
    body.totalScore = 0;
    body.taskScore = {};
    body.task = TASKS[0];
    body.taskType = "high";

    client.search({
        index: "user",
        type: "_doc",
        body: {
            "aggs": {
                "count": {
                    "terms": {
                        "field": "taskType",
                        "size": 99999
                    }
                }
            }
        }
    }, function (err, result) {
        if (err)
            return next(err);

        let found = {low: false, control: false, high: false};
        let min = Number.MAX_VALUE;
        let minV;

        for (let e of result.aggregations.count.buckets) {
            if (min > e.doc_count) {
                min = e.doc_count;
                minV = e.key;
            }

            found[e.key] = true;
        }

        for (let k in found) {
            if (found[k] === false) {
                minV = k;
            }
        }

        body.taskType = minV;


        client.index({
            index: "user",
            type: "_doc",
            body
        }, (err, result) => {
            if (err)
                return next(err);


            res.cookie('login', result._id, {maxAge: 99999999999999999, path: "/"});
            res.send(result._id);
        });
    });
});


const auth = express();


auth.all('/restricted/*', authFnc);
auth.all('/backend/*', authFnc);

function authFnc(req, res, next) {
    if (req.originalUrl.includes("register"))
        return next();
    let cookies = req.cookies;

    if (!cookies || cookies.login == null)
        return res.redirect('/public/register.html');

    else {
        req.userId = cookies.login;
        next();
    }
}

// load frontend
auth.use(express.static(__dirname + '/../frontend'));


bknd.getAsync('/search', async (req, res) => {
    /**
     * @type SearchRequest
     */
    let query = req.query.query;
    let task = +req.query.task;


    /**
     * @type {User}
     */
    let user = await getUserInfo(req);

    // noinspection EqualityComparisonWithCoercionJS
    if (task != user.task)
        throw new Error("Invalid task");


    /**
     * @type {SearchResponse<Page>}
     */
    let pageDoc = await client.search({
        "index": "page",
        "type": "_doc",
        "size": SEARCH_SIZE,
        body: {
            "query": {
                "match": {
                    "body": query
                }
            }
        }

    });

    let taskIndex = TASKS.indexOf(task);

    if (taskIndex > MAX_TASK)
        return res.status(404).send('Not found');

    let score = 0, hitT = 1, cnt = 1;
    let hits = pageDoc.hits.hits.map(e => {
        if (e._source.taskScore && e._source.taskScore[task])
            score += hitT++ / cnt;
        cnt++;
        e._source.id = e._id;
        return e._source;
    });

    score /= SEARCH_SIZE;

    let taskScore = {};
    taskScore[task] = score;
    await client.update({
        index: 'user',
        type: '_doc',
        id: req.userId,
        body: {
            doc: {
                taskScore,
                task: TASKS[taskIndex + 1]
            }
        }
    });

    res.send({averageScore: score, results: hits});
});

bknd.getAsync('/task', async (req, res) => {
    let user = await getUserInfo(req);


    if (user.task >= TASKS[TASKS.length - 1])
        return res.status(404).send('Not found');

    let taskDoc = await client.get({
        index: 'task',
        type: '_doc',
        id: user.task
    });


    /**
     * @type {Task}
     */
    let task = taskDoc._source;
    task.id = taskDoc._id;

    if (task.id >= SUGGESTIONS_RANGE[0] && task.id <= SUGGESTIONS_RANGE[1])
        switch (user.taskType) {
            case "high":
                task.suggestions = getTop3(task.id);
                break;
            case "low":
                task.suggestions = getBot3(task.id);
                break;
        }

    res.send(task);
});

auth.use('/backend', bknd);
app.use(auth);

app.listen(port, "10.0.0.4", () => console.log(`Example app listening on port ${port}!`));


/// ============== Used classes

/**
 * @class Page
 * @property {string} id
 * @property {Date} timestamp
 * @property {string} headline
 * @property {string} body
 */

/**
 * @class PageDoc
 * @property {string} id
 * @property {Date} timestamp
 * @property {string} category
 * @property {string} headline
 * @property {string} body
 * @property {string} source
 * @property {int} year
 */

/**
 * @class SearchResult
 * @property {Page} page
 * @property {string} summary
 * @property {number} score
 */

/**
 * @class SearchResults
 * @property {SearchResult[]} results
 * @property {number} averageScore
 */

/**
 * @class Task
 * @property {string} description
 * @property {string} narrative
 * @property {string} title
 * @property {{term, avp}[]|undefined} suggestions
 * @property {string} id
 */

/**
 * @class Suggestion
 * @property {string} query
 * @property {number} score
 */


/**
 * @class SearchRequest
 * @property {string} query
 * @property {string} taskId
 */

/**
 * @class RegisterRequest
 * @property {string} nickname
 * @property {string} profession
 * @property {Date} dateOfBirth
 * @property {char} sex
 */

/**
 * @class User
 * @extends RegisterRequest
 * @property {string} cookie
 * @property {string} taskType
 * @property {date} create
 * @property {{}} taskScore
 * @property {number} totalScore
 * @property {int} task
 */

/**
 * @class AutocompleteRequest
 * @property {string} query
 */

